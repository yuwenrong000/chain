package com.gupaoedu.vip.pattern.chain.general;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tom.
 */
public class Client {

    public static void main(String[] args) {
        List<Integer> param = new ArrayList();
        param.add(1);
        param.add(2);
        param.add(3);
        param.add(4);

        Handler handlerA = new ConcreteHandlerA();
        handlerA.setNextHanlder(new ConcreteHandlerB());
        handlerA.handleRequest(param);


        System.out.println(param);
    }

    //抽象处理者
    static abstract class Handler {

        protected Handler nextHandler;

        private void setNextHanlder(Handler successor) {
            this.nextHandler = successor;
        }

        public abstract void handleRequest(List<Integer> list);

    }

    //具体处理者A
    static class ConcreteHandlerA extends Handler {

        @Override
        public void handleRequest(List<Integer> list) {

            // 用list 来过滤

            list.removeIf(it -> it == 1);
            System.out.println(list);
            if (this.nextHandler != null) {
                this.nextHandler.handleRequest(list);
            }

        }

    }

    //具体处理者B
    static class ConcreteHandlerB extends Handler {

        @Override
        public void handleRequest(List<Integer> list) {
            list.removeIf(it -> it ==2);
            System.out.println(list);
            if (this.nextHandler != null) {
               this.nextHandler.handleRequest(list);
            }

        }
    }
}
